#!/bin/bash

read X 
read Y 
read Z 

if [ $X = $Y ] && [ $Y = $Z ]; then
    answer="EQUILATERAL"
elif [ $X = $Y ] || [ $X = $Z ] || [ $Y = $Z ]; then
    answer="ISOSCELES"
else
    answer="SCALENE"
fi

echo -n $answer
